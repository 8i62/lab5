﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Motors.BL;

namespace Form01
{
    public partial class MainForm : Form
    {
        private Park park;
        private Sumary sum = new Sumary();

        public MainForm(Park temp)
        {
            InitializeComponent();

            park = temp;

            Upd_list();
            Upd_sum();
        }

        private void button_del_Click(object sender, EventArgs e)
        {
            if (listbox_vehicles.SelectedIndex != -1)
            {
                park._auto.RemoveAt(listbox_vehicles.SelectedIndex);
                listbox_vehicles.Items.RemoveAt(listbox_vehicles.SelectedIndex);
                Upd_sum();
            }
        }

        private void Upd_list()
        {
            for (int i = 0; i < park._auto.Count; i++)
                listbox_vehicles.Items.Add(item: park._auto[i].Name + " - " + park._auto[i].Price.ToString());
        }

        private void Upd_sum()
        {
            label_sum.Text = "Summary = " + sum.Sum(park).ToString();
        }

        private void button_edit_Click(object sender, EventArgs e)
        {
            if (listbox_vehicles.SelectedIndex >=0)
            {
                Form edit = new Add_Edit(listbox_vehicles.SelectedIndex, park);
                edit.Show();
                this.Hide();
            }
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            Form edit = new Add_Edit(park);
            edit.Show();
            this.Hide();
        }
    }
}
