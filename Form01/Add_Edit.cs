﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Motors.BL;

namespace Form01
{
    public partial class Add_Edit : Form
    {
        private Park parkone;
        private int number;
        private string startclass;

        public Add_Edit(int numb, Park p)
        {
            InitializeComponent();
            parkone = p;
            number = numb;
            startclass = parkone._auto[number].GetType().ToString();

            textBox_name.Text = parkone._auto[number].Name;
            textBox_price.Text = parkone._auto[number].Price.ToString();

            _FillClasses();
        }

        public Add_Edit(Park p)
        {
            InitializeComponent();
            parkone = p;
            startclass = "";
            number = -1;

            _FillClasses();
        }

        private void _FillClasses()
        {
            comboBox_class.Items.Add("Cars");
            comboBox_class.Items.Add("Motorcycles");
            comboBox_class.Items.Add("Electric");

            if (number == -1)
                comboBox_class.SelectedIndex = 0;

            else
            {
                if (parkone._auto[number].GetType().ToString() == "Motors.BL.Motorcycle")
                    comboBox_class.SelectedIndex = 1;

                else if (parkone._auto[number].GetType().ToString() == "Motors.BL.Electric")
                    comboBox_class.SelectedIndex = 2;

                else
                    comboBox_class.SelectedIndex = 0;
            }
        }
        
        private void button_ok_Click(object sender, EventArgs e)
        {
            if (number == -1)
            {
                parkone.AddCar(new Vehicle());
                number = parkone._auto.Count-1;
            }

            if(parkone._auto[number].GetType().ToString() != startclass)
            {
                    ReplaceVehicle(comboBox_class.SelectedIndex);
            }

             parkone._auto[number].Name = textBox_name.Text;



            try
            {
                if (Int32.Parse(textBox_price.Text) > 0)
                {
                    parkone._auto[number].Price = Int32.Parse(textBox_price.Text);
                    Form main = new MainForm(parkone);
                    main.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Неверное число");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Неверное число");
            }
        }

        private void ReplaceVehicle(int index)
        {
            if (index == 0)
            {
                parkone._auto[number] = parkone.carmaker.CreateVehicle();
            }

            else if (index == 1)
            {
                parkone._auto[number] = parkone.motocyclemaker.CreateVehicle();
            }

            else
            {
                parkone._auto[number] = parkone.electricmaker.CreateVehicle();
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            Form main = new MainForm(parkone);
            main.Show();
            this.Close();
        }
    }
}


