﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Motors.BL;

namespace Form01
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Park park = new Park();

            Vehicle car1 = park.carmaker.CreateVehicle();
            Vehicle car2 = park.motocyclemaker.CreateVehicle();
            Vehicle car3 = park.electricmaker.CreateVehicle();

            park.AddCar(car1);
            park.AddCar(car2);
            park.AddCar(car3);

            Application.Run(new MainForm(park));
        }
    }
}
