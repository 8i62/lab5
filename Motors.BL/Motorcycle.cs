﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class Motorcycle : Vehicle
    {
        private int _pistons;
        private int _maxspeed;

        public int Pistons
        {
            get { return _pistons; }
            set { _pistons = value; }
        }

        public int Maxspeed
        {
            get { return _maxspeed; }
            set { _maxspeed = value; }
        }
    }
}
