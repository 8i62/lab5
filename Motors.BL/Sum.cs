﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class Sumary
    {
        public int Sum(Park p)
        {
            int price=0;

            for (int i = 0; i < p._auto.Count; i++)
                price += p._auto[i].Price;

            return price;
        }
    }
}
