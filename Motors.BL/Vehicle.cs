﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class Vehicle
    {
        private string _name;
        private int _price;
        private double _weight;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public string Nameturn()
        {
            return this.GetType().ToString();
        }
    }
}
