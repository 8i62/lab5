﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class Electric : Car
    {
        private string _battery;
        private int _resource;

        public string Battery
        {
            get { return _battery; }
            set { _battery = value; }
        }

        public int Resource
        {
            get { return _resource; }
            set { _resource = value; }
        }
    }
}
