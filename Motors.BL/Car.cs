﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class Car : Vehicle
    {
        private int _doors;
        private int _chairs;

        public int Doors
        {
            get { return _doors; }
            set { _doors = value; }
        }

        public int Chairs
        {
            get { return _chairs; }
            set { _chairs = value; }
        }
    }
}
