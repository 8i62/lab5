﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class MotorcycleMaker : IVehicleMaker
    {
        public Vehicle CreateVehicle()
        {
            Vehicle vehicle;
            vehicle = new Motorcycle();
            vehicle.Price = 3000;
            vehicle.Name = "Motorcycle";
            return vehicle;
        }
    }
}
