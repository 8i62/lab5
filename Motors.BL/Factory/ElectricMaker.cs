﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class ElectricMaker : IVehicleMaker
    {
        public Vehicle CreateVehicle()
        {
            Vehicle vehicle;
            vehicle = new Electric();
            vehicle.Price = 9000;
            vehicle.Name = "Electric";
            return vehicle;
        }
    }
}
