﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public interface IVehicleMaker
    {
        Vehicle CreateVehicle();
    }
}
