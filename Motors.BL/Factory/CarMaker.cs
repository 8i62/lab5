﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class CarMaker : IVehicleMaker
    {
        public Vehicle CreateVehicle()
        {
            Vehicle vehicle;
            vehicle = new Car();
            vehicle.Price = 5000;
            vehicle.Name = "Car";
            return vehicle;
        }
    }
}
