﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motors.BL
{
    public class Park
    {
        public List<Vehicle> _auto = new List<Vehicle>();
        private int _sum = 0;


        public IVehicleMaker carmaker = new CarMaker();
        public IVehicleMaker motocyclemaker = new MotorcycleMaker();
        public IVehicleMaker electricmaker = new ElectricMaker();



        public void AddCar(Vehicle temp_car)
        {
            _auto.Add(temp_car);
            _sum += temp_car.Price;
        }

        public int Sum
        {
            get { return _sum; }
        }
    }
}
